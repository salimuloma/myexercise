# Write a Python program that returns true if the two given integer values are equal
#  or their sum or difference is 5.

# define a function 

def myFunction(x,y):
    # check for these conditions 
    # 1. x==y
    # 2. x+y=5
    # 3. x-y=5

    if x==y or (x+y)==5 or abs(x-y)==5:
        return True
    
    else:
        return False
    
# call the function and test it with some values
print(myFunction(7,2)) 
print(myFunction(2,2))
print(myFunction(2,3))

# all the above return True